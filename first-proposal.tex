\documentclass[a4paper,11pt]{article}

% Document tiré de https://www.monperrus.net/martin/template-latex-anr,
% adapté à l'AAP 2019 JCJC
% en particulier de la trame LibreOffice proposée:
%   http://www.agence-nationale-recherche.fr/fileadmin/aap/2019/aapg-anr-2019-pre-proposition.docx

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
%% Sans-serif Arial-like fonts
\renewcommand{\rmdefault}{phv}
\renewcommand{\sfdefault}{phv}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage{xspace}
\usepackage[colorlinks,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}
\setlength{\footskip}{2cm}
\setlength{\headheight}{2cm}
\usepackage[lmargin=1.9cm,rmargin=1.9cm,tmargin=3cm,bmargin=1cm,includefoot]{geometry}
\usepackage{lastpage}%% to get the total number of pages
\usepackage{expdlist} % '\compact' option

\usepackage{pdflscape}

\newcommand{\projectname}[0]{\textsc{Coqola}\xspace}
\newcommand{\projectduration}[0]{4 years}
\newcommand{\projectcost}[0]{\EUR{350,000}}
\newcommand{\coordinator}{OCamlPro}
\newcommand{\selectioncommittee}%
  {CES 48 - Fondements du numérique : informatique, automatique, traitement du signal}

\usepackage{amsmath}
\usepackage{amssymb}

% more compact paragraph style
\renewcommand{\paragraph}[1]{\vspace{\baselineskip}\textbf{#1}.}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\setlength{\headheight}{80pt}
\fancyhead[C]{\includegraphics[height=1cm]{anr-logo/ANR-logo.pdf}
\hfill
\projectname{}
\hfill
PRCE
\\AAPG-ANR-2024
\hfill
Cordonné par: \coordinator
\hfill
\projectduration, \projectcost
\\
\selectioncommittee
}
\cfoot{\thepage}

\newcommand{\content}[1]{\emph{#1}\\}

\usepackage{natbib}
\bibliographystyle{plainnaturl}
\citestyle{authoryear}

% http://nw360.blogspot.com/2007/12/rename-bibliography-title-in-latex.html
\renewcommand\refname{\vspace{-1cm}}

%\usepackage{xcomment}
\newenvironment{xcomment}{\em}{}

\begin{document}

\begin{center}
  {\huge
    \projectname{}: Coq Optimisé par son LAngage
  }\\[1em]

  {\large
    Principal Investigator (PI): \coordinator, entreprise participante
  }
\end{center}

% \begin{xcomment}
%   Utilisez une mise en page permettant une lecture confortable du
%   document (page A4, calibri 11 ou équivalent, interligne simple,
%   marges 2 cm ou plus, numérotation des pages).

%   Le site de dépôt refuse le téléchargement d’un document de plus de 4 pages ou dans un format autre que pdf. Aucune annexe n’est acceptée. Les CV du coordinateur ou de la coordinatrice et des responsables scientifiques des éventuels partenaires sont à compléter en ligne à partir du site de dépôt IRIS. Dans le cadre d’un projet JCJC ou PRME, seul le CV du coordinateur ou de la coordinatrice est requis.

%   La rédaction de votre pré-proposition doit permettre son évaluation selon les 2 critères d’évaluation définis dans le texte et dans le guide de l’AAPG2024 : « Qualité et ambition scientifique » et « Organisation et réalisation du projet ». Il est nécessaire de se reporter au guide afin d’en connaitre les sous-critères, différenciés selon l’instrument de financement choisi.

% Aucune donnée financière n’est requise dans le corps de la pré-proposition, aucun sous-critère d’évaluation n’étant lié au budget en étape 1.

% L’enregistrement d’un projet PRCI ne nécessite pas la rédaction d’une pré-proposition auprès de l’ANR mais uniquement la complétion du formulaire et des CV sur le site IRIS.
% \end{xcomment}

\vspace{-2em}
\section{Contexte, positionnement et objectif(s) de la pré-proposition}
% Pre-proposal’s context, positioning and objective(s)

% \begin{xcomment}
% Dans cette section, répondant au critère d’évaluation « Qualité et ambition scientifique », doivent être présentées les informations suivantes :
% \begin{itemize}
% \item Description des objectifs et des hypothèses de recherche ;
% \item Positionnement du projet par rapport à l’état de l’art ;
% \item Présentation de la méthodologie utilisée pour atteindre les objectifs du projet, description précise de la ou les méthodes envisagées en incluant la couverture disciplinaire (mono-trans-inter-disciplinaire) ;
% \item Plus-value du projet en termes d’apport scientifique, que ce
%   soit en termes d’objet, de problématique et d’approche
%   méthodologique, et plus-value en termes de production de
%   connaissances\footnote{Quel que soit le type de projet déposé :
%     projet visant des objectifs ou concepts originaux, en rupture ou
%     exploratoires ; projet visant la levée de verrous scientifiques
%     bien identifiés dans la communauté ; projets exploitant les
%     données générées par les infrastructures de recherche ; projets
%     faisant suite à de précédents projets et permettant d’envisager de
%     nouveaux objectifs. Préciser au besoin les résultats préliminaires
%     obtenus.} ;
% \item Positionnement du projet par rapport aux enjeux de recherche de
%   l’axe scientifique choisi.
% \end{itemize}

% Le critère « Qualité et ambition scientifique » est un critère discriminant, i.e. seuls les projets ayant obtenu la note « A » sur ce critère de la part du comité d’évaluation scientifique peuvent être invités en étape 2.
% \end{xcomment}

\paragraph{Proof assistants}
This proposal concerns the Coq proof assistant. Proof assistants are
software systems dedicated to letting computer verify the correctness
of a mathematical proof; their user expresses a mathematical statement
of interest, and provides proof arguments in a format that the system
can understand and verify, with support and automation provided by the
software. The statement verified can be mathematical theorems, but
also statements of the form ``the program $P$ satisfies the
specification $S$'', providing an approach to verified programming --
programs that come with a computer-checked proof that they respect
their specification.

Proof assistants are ground-breaking software systems, realizing an
ideal that mathematicians thought impossible during most of the
twentieth century: getting full confidence that a proof argument is
correct, up to the logical axioms (which have to be trusted). The
twentieth century has seen impressive verification efforts for both
mathematical statements (for example the odd-order theorem, or the
formal proof of the Kepler conjecture) and software (the CompCert
C compiler, the SeL4 micro-kernel). The first proof assistants where
designed in the 1970s, but they are increasing in popularity in the
twenty-first century and seeing constant advances in ergonomics,
automation (with promising help from machine learning research), and
adoption among researchers in both computer science and
mathematics. One can hope to see proof assistants become a standard
tool among mathematicians this century, making uncertainty about the
correctness of a published paper a thing of the past, as well as part
of the standard methodology to implement safety-critical software
systems.

\paragraph{Performance-critical, but poorly understood performance}
Coq is among the proof assistants based on type theory (along with
Agda, Lean, Dedukti...), which are designed like programming languages
with a very advanced type system: proofs are programs in these
systems, their verification corresponds to static type checking, and
proof automation is done through various forms of
meta-programming. When used for ambitious verification projects, these
proof assistants are very resource intensive in both memory and
compute time. It is not uncommon for proof assistant users to run out
of memory even on powerful desktop machines, or to wait minutes or an
hour to know if a given modification that they performed does not
break the proof.

The compute workload of a proof assistant is dominated by symbolic
term manipulation, so it is very different from typical
resource-intensive workload studied in program performance research,
such as scientific computing or web servers. We know relatively little
about how to improve the efficiency of those proof assistants;
existing works suggest that better algorithms can make
a difference~\citep*{jason-gross-thesis}, but also that proof
assistants are tightly coupled to their implementation language: Coq
needs have informed implementation choices of OCaml and
conversely~\citep*{vm-compute,native-compute}, Lean resulted in
specific language runtime research~\citep*{mimalloc,perceus}, Isabelle
uses PolyML, its own dedicated SML dialect and implementation.


\paragraph{Our project: dual-expertise research with clean interfaces}
Coq is implemented in the OCaml programming language, and is arguably
one of the most, maybe the most important contribution of OCaml to the
scientific community.

The goal of \projectname{} is to reunite experts of the Coq
implementation and experts of the OCaml compiler and runtime, to
research improvements to the Coq proof assistant that require language
expertise, and improvements to OCaml that benefit
Coq and other symbolic manipulation systems. This area of confluence is fertile ground for new programming-languages and proof-assistant research.

Such joint work was common in the 1990s, when the OCaml and Coq
development team were closely related, part of the same research
groups. A striking result was \citet*{vm-compute}, a custom variant of
the OCaml bytecode interpreter designed for the needs of Coq. As the
two communities grew and specialized, the interactions became less
frequent and the joint research rarer. In some respects this became
a form of technical and scientific debt for the Coq software system,
with some parts that were tightly coupled to the OCaml runtime, but
without the expertise among Coq developers to maintain it and keep it
compatible with newer OCaml versions where the runtime had changed.

To avoid repeating the difficult situations of the past, we propose to
focus on new research that is beneficial to Coq but not Coq-specific,
that can be adopted (if succesful) by other projects and maintained by
a larger community of users. Any coupling between the proof assistant
and the language, compiler or runtime system should also go through
a clear and well-documented interface, which can be maintained in
future versions of OCaml.

\paragraph{Objectives} More precisely, we have the following
objectives in mind.

\begin{enumerate}
\item \emph{Profiling}. It is sometimes difficult to know where Coq is
  spending time when checking complex proofs, as we lack profiling
  methods that scale to a real-world proof assistant. We propose to
  explore fine-grained profiling to better understand the performancee
  impact of its programming choices and algorithms. Memory profiling
  is an even bigger issue, in part due to the paucity of tools
  (sampling-based memory profilers such as StatMemprof show us what is
  present in the memory, but not why), and also due to unusual data
  patterns with in particular extremely high degrees of sharing among
  proof terms. We propose to research domain-specific profiling tools
  to better underestand memory usage of symbolic manipulation tools,
  and possibly consider different memory representations of proof
  terms. Besides improving the Coq project and the OCaml community,
  the skills built through this objective will bring business value to
  OCamlPro as an OCaml consulting company.

\item \emph{Optimizer}. OCamlPro developed the \texttt{flambda}
  optimizer for OCaml, first released in 2016. It has shown promising
  results for Coq (a 20\% performance improvement on average), but was
  never tuned to Coq needs and was difficult to use on meta-program
  outputs due to higher compilation times. We are working on a more
  modular and expressive \texttt{flambda2}
  optimizer~\citep*{flambda2}, which is still in prototyping phase. We
  propose to study the impact of \texttt{flambda2} on the Coq codebase
  and improve its optimizations to benefit Coq and other OCaml
  programs in this problem domain. Such an evaluation on significant
  open-source codebases is a prerequisite for a planned scientific
  publication on \texttt{flambda2}.

\item \emph{Runtime support}. Would specialized runtime services
  benefit proof assistants? Certain resource-critical programs
  implemented in functional programming languages have benefited from
  a dedicated, separate memory region for long-term data that is not
  traversed by a garbage collector~\citep*{compact-regions}. Coq
  implementors wonder if such a dedicated region for proof-term could
  also perform hash-consing (deduplication) on the fly. A precedent is
  Poly/ML support for hash-consing during serialization, which rarely
  improves general programs but is effective for the Isabelle proof
  assistant.

% % Removed: [Gabriel] I heard that Enrico Tassi is already working
% % on this, so no need to step on his toes.
%
% \item \emph{Parallelism and effect handlers}. Coq has support for
%   multi-process parallelism~\citep*{pide}. OCaml 5 gained a multicore
%   runtime that enables multithreaded programs. We would be interested
%   in prototyping the use of Multicore OCaml within Coq; this would
%   have the benefit of using shared memory instead of requiring
%   exchanging proof terms, which induce non-neglectible communication
%   overhead. A prototype would let us gather performance results and
%   inform future design, engineering and research directions.
%
%   The OCaml multicore project introduced effect handlers
%   (delimited control operators) to build cooperative concurrency as
%   user libraries. We would like to explore usage of effect handlers in
%   the Coq implementation, as part of its concurrency story or for
%   other aspects such as proof search.

\item \emph{Data representation}. There is a active strand of research
  on more memory-efficient data representations for
  OCaml~\citep*{unboxed-constructors, knit-frog}. These experiments
  often use examples and prospective applications drawn from numeric
  computing or general-purpose data structures. Based on our study of
  the memory profile of Coq (\emph{Profiling} above), we propose to
  design and prototype data-representation optimizations for OCaml
  datatypes, that could benefit Coq and other symbolic programs. This
  could provide the impetus to integrate a representation-description
  language inspired by \citep*{knit-frog} in the OCaml compiler,
  a significant research objective.

\item \emph{Runtime interfaces} As we previously mentioned, some parts
  of Coq, notably its strong reduction virtual
  machine~\citep*{vm-compute}, rely on internal details of the OCaml
  runtime, and are proving a maintenance bottleneck: when OCaml
  changes, this code breaks and Coq maintainers do not have the
  expertise anymore to maintain it easily. On the research side, we
  propose to design an abstraction boundary around the internal
  constructs that Coq relies on, a runtime environment for
  Just-In-Time code production; this could start a long-term research
  collaboration and/or a PhD topic. On the engineering side, getting
  an agreement from the Coq and OCaml team on this low-level interface
  could greatly lower maintenance costs.

\item \emph{An untyped input language for the OCaml compiler.} Coq
  compiles Coq programs into OCaml source programs -- for example, the
  CompCert C compiler is compiled from Coq to OCaml, and then by the
  OCaml compiler. A recurrent difficulty is that Coq programs use
  a more powerful type system as OCaml, so the compilation scheme
  keeps using escape hatches to disable the OCaml type system. The
  \texttt{native\_compute} machinery also performs just-in-time
  compilation into OCaml for type-level computations, and it wants to
  use data-representation tricks that cannot be safely expressed in
  OCaml source. Recently Coq developers have proposed to target not
  OCaml directly, but an untyped representation used within the OCaml
  compiler, through Malfunction project~\citep*{malfunction} which
  provides a source syntax -- and, more importantly, a well-defined
  semantics -- for this intermediate representation. Malfunction is
  only a prototype and rapidly showing limits. We propose to extend
  Malfunction to a robust alternative input language for the OCaml
  compiler toolchain, and (long-term) consider its integration in the
  compiler distribution. This raises interesting issues of language
  and compiler design, and could be useful for other research projects
  currently producing OCaml code. This could be an avenue of
  collaboration with the MetaCoq project that formalized the current
  Malfunction fragment to prove correctness of Coq extraction
  (\href{https://github.com/yforster/coq-verified-extraction/}{coq-verified-extraction}),
  and could be combined with ongoing research on OCaml compilation
  passes~\citep*{tmc} to obtain some correctness results for the OCaml
  compiler internals.
\end{enumerate}

All software produced as outcomes of this research will be released
publicly as free software, following the licences practices of the
upstream projects (Coq or OCaml depending on the contribution).

\section{Partenariat (consortium ou équipe)}
% Partenariat

% \begin{xcomment}
% Cette section est en lien avec le critère d’évaluation « Organisation et réalisation du projet » .

% Présenter le coordinateur / la coordinatrice scientifique, son expérience antérieure dans le domaine objet de la pré-proposition, son implication dans le projet objet de la pré-proposition y compris son taux d’implication.

% Présenter le consortium et sa complémentarité, l’implication de chaque partenaire dans l’atteinte des objectifs : Indiquer les différentes compétences envisagées pour mener le projet objet de la pré-proposition, en précisant l’identité des scientifiques impliqués, l’identification des établissements auxquels ils ou elles sont rattaché.e.s et tout autre élément permettant de juger de la qualité et complémentarité des partenaires et du caractère effectif de la collaboration.
% \end{xcomment}

\subsection{Project Coordinator: OCamlPro} OCamlPro is an OCaml consulting company with a R\&D focus and extremely strong expertise on the OCaml compiler. In particular, OCamlPro wrote \texttt{flambda}, an advanced optimizer middle-end, and is actively working on its replacement \texttt{flambda2}. OCamlPro has built various programming support tools, including the first memory profiler for OCaml programs, ocp-memprof.

\paragraph{Participants} all participants below are R\&D engineers, co-authors of the \texttt{flambda2} optimizer.
\begin{description}[\compact]
% \item[Fabrice LE FESSANT] Founder of OCamlPro, former Fulltime Researcher at Inria, member of the OCaml coreteam, expert in languages, compilation and distributed systems;
\item[Vincent LAVIRON] Expert in abstract interpretation, co-author of \texttt{flambda}, co-maintainer of the OCaml compiler;
\item[Basile CLÉMENT] PhD in compiler optimizations verified in Coq, co-maintainer of the Alt-Ergo prover;
\item[Guillaume BURY] PhD in automated theorem proving, expertise in efficient implementation of automated theorem provers (SAT, SMT, first-order), co-maintainer of Zenon and Alt-Ergo provers;
\item[Pierre CHAMBART] author of \texttt{flambda}, PhD in
  algorithms, co-supervised a PhD thesis on memory profiling in OCaml,
  co-maintainer of the OCaml compiler;
\item[Nathanaëlle COURANT] (25\%) PhD in efficient implementations of dependent type
  theories verified in Coq, expertise in compiler optimizations verified in Coq;
\end{description}
\vspace{-1em}

% Total: 150% sur 4 ans, donc 6 personnes-années.

\paragraph{Contributions} OCamlPro will bring its strong expertise of the OCaml compiler, runtime system, optimization opportunities and profiling tools. The total time commitment proposed is one person-year for each objective, so 6 person-years in total.

\subsection{Academic partner: INRIA} INRIA is the birthplace of both the OCaml programming language and the Coq proof assistant. Those open source projects have since had a vibrant life with many external contributors, but core maintainers remain INRIA researchers and engineers. (Participants listed below are researchers (CR), except Gaëtan Gilbert who is a research engineer.)
\begin{description}[\compact]
\item[Gaëtan Gilbert] (20\%) PhD in proof assistants, co-maintainer of the
  Coq proof assistant, expertise in Coq performance as well as its
  benchmarking and Continuous Integration systems,
\item[Guillaume Munch-Maccagnoni] (15\%) PhD in type theory, current
  research on the OCaml runtime, memory profiling (\href{https://gitlab.com/gadmm/memprof-limits/}{memprof-limits}), and concurrent
  OCaml programs
\item[Pierre-Marie Pédrot] (30\%) PhD in type theory, co-maintainer of
  the Coq proof assistant, expertise in performance, profiling and
  optimization of Coq.
\item[Gabriel Scherer] (20\%) PhD in type systems, co-maintainer of
  the OCaml compiler, current research on the OCaml runtime and data
  representation optimizations
\end{description}
\vspace{-1em}

% Total: 85% sur 4 ans, donc 3.4 personnes-années (40.8 mois).
\paragraph{Contributions}: INRIA researchers and research engineers
bring their expertise on the Coq design and implementation. They will
also play a leading role in presenting and disseminating the
scientific results of the project, as well as leading the upstreaming
of new OCaml and Coq improvements for long-term impact.

\section{Bibliographie}
% References related to the project

% \begin{xcomment}
% Cette section est en lien avec le critère d’évaluation « Qualité et ambition scientifique ».

% Liste des références bibliographiques de la pré-proposition relative à l’état de l’art.

% Veillez à renseigner des références « utilisables », i.e. incluant les premiers co-auteurs et co-autrices, le titre complet, le titre de la revue, l’année, etc. Vous pouvez compléter ces références par le lien « accès ouvert » (« open access ») des articles pour améliorer leur accessibilité aux évaluateurs et évaluatrices.
% Les preprints sont acceptés, en particulier pour le référencement de données préliminaires.

% La bibliographie doit être comptabilisée dans la limite des 4 pages de la pré-proposition.
% \end{xcomment}

\begin{small}
\nocite{}
\setlength{\bibsep}{2pt}
\bibliography{proposal.bib}
\end{small}

\newpage

% \section{Volet financier}

% À retirer de la version finale de la pré-proposition, mais utile pour
% discuter les détails.

% \begin{verbatim}
% Demande OCamlPro:
% - 6 sujets, une personne-année par sujet => 600K€ de dépenses
%   on facture 1/3 à l'ANR => 200K€ de budget demandé
%   + 10\% overhead => 220K€ de budget demandé

% Demande INRIA:
% - one year of post-doc or engineer position (70K€)
% - one master intern (5K€)
% - 15K€ of equipment (laptops + a benchmarking machine)
% - 22K€ of general expenses (missions): 5K€/year * (4*85% + 1)
% => 112K€ total
% => 130K€ after overhead (assuming 16% overhead)

% Total: 220 + 130 = 350

% Splitting between INRIA Paris and INRIA Rennes:
% - the post-doc position goes to INRIA Rennes: 70K€
% - 3K€ equipement in Paris, 12K€ in Rennes
% - the master intern to INRIA Paris: 5K€
% - 5K€ expenses in Paris, 17K€ in Rennes
%
% Total split: 99K€ in Rennes, 13K€ in Paris.
% With overhead (16%): 115K€ in Rennes, 15K€ in Paris.
% \end{verbatim}


\end{document}
